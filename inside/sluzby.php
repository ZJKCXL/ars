<script>
var toChange = {
  selector: '.tinymce-2change',
  entity_encoding : "raw",
  menubar: false,
  inline: true,
  plugins: "save",
  toolbar: false,
  setup: function (editor) {
        editor.on('change', function () {
           //  editor.save();
           content = editor.getContent();
           //console.log('save >' + editor.id  + '>' + content);
           $.ajax({
             type:       'POST',
             cache:      false,
             url:        '/inside/ajax-save.php?name=' + editor.id,
             data:       'tinydata=' + content,
             success:    function(dt, status, request) {
               // console.log(request.getAllResponseHeaders());
            }
        });
        });
    }
};
tinymce.init(toChange);
</script>


<?php
 /*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */
$info_text = "";

$time = time();
$file_name = date("d_m_y", $time) . "TZ" . $time;
$file_name2 = date("d_m_y", $time) . "TZ" . $time;
$table_name = "tbl_services";
$page_name = "sluzby";
$deletext = "Položka byla smazána.";
$updatext = "Položka byla opravena.";
$addtext = "Položka byla přidána.";
$jeho = "Položku";
?>
<h1>Služby</h1>
<a href="index.php?id=<?php echo $page_name; ?>new<?php if ($type) {echo "&amp;type=" . $type;}?>" class="fas fa-plus-circle"><span>Přidat další</span></a>
 
<?php

$price =   str_replace (  ',' , '.' , trim($_POST['servicePrice']) );

if ($_REQUEST['deletemecomp'] > 0) {

    $delquery = 'DELETE FROM `tbl_services` WHERE `tbl_services`.`ID` =   ' . $_REQUEST[deletemecomp] ;
    $delres =  $GLOBALS["link"]->query($delquery);
}
if ($_REQUEST['delete'] > 0) {
    $delquery = 'DELETE FROM `tbl_services` WHERE `tbl_services`.`ID` =   ' . $_REQUEST[delete] ;
    $delres = $GLOBALS["link"]->query($delquery);
}
$supercount = 0;
if (isset($_REQUEST["send"]) && isset($_REQUEST["newsid"]) && is_numeric($_REQUEST["newsid"]) && $_REQUEST["newsid"] > 0) {
    if (strlen(trim($_GET["newsid"]))) {
        $query = "Update " . $table_name . " set ";
       
        $superquery = "SHOW FULL COLUMNS FROM `".$table_name."`";
        $superes = $GLOBALS["link"]->query($superquery);
        if ($superes && mysqli_num_rows($superes) > 0) {

            while ($supeRow = mysqli_fetch_array($superes)) {
                if(isset($_POST[$supeRow[Field]])) {
                $col_query  =  $supeRow[Field] ;
                $val_query  =  trim(strip_tags($_POST[$supeRow[Field]])) ;
                if($supercount > 0) { $query .= ", ";  } 
                $supercount++;
                $query .= $col_query . " = '" . $val_query . "'";
                }
   
            }
        }
 
        $query .= " where ID = " . $_REQUEST["newsid"];

        $res = @$GLOBALS["link"]->query($query);
        if ($res && @mysqli_affected_rows($link) > 0) {
            $info_text .= "Položka úspěně uložena.";
            $alertype = " alert-success ";
        } else {
            $info_text .= "Položka nebyla změněna.";
            $alertype = " alert-danger ";
        }
    } else {
        $info_text .= "Chyba během ukládání Položky. Nebyly zadány všechny povinné parametry";
        $alertype = " alert-danger ";
    }

} elseif (isset($_REQUEST["send"]) && !isset($_REQUEST["newsid"])) {


  

  $query = "Insert Into " . $table_name . " ( `serviceDesc` , `serviceNr`, `serviceType`  , `serviceOrder` , `serviceCode`, `serviceUnit`, `servicePrice`, `servicePriceType`, `serviceCategory`)
            VALUES (  '". $_POST['serviceDesc']."' , '". $_POST['serviceNr']."', '". $_POST['serviceType']."' , '0' , '" . $_POST['serviceCode'] . "', '" . $_POST['serviceUnit'] . "', '" . $price . "', '" . $_POST['servicePriceType'] . "', '" . $_POST['serviceCategory'] . "')";
    $res = $GLOBALS["link"]->query($query);
    if ($res && mysqli_affected_rows($link) > 0) {
        $info_text .= "Položka úspěně uložena.";
        $alertype = " alert-success ";
    } else {
        $info_text .= "Položka nebyla uložena.";
        $alertype = " alert-danger ";
    }

} else {

}

if (@$info_text != "") {
    ?>
        <div class="alert <?php echo $alertype; ?>" role="alert"><?php echo $info_text; ?></div>
        <?php
}
?>
 <div class="table-responsive">
<table  id='tableOUT' class="display  table table-striped table-bordered m-b-0 toggle-circle footable-loaded footable tablet breakpoint demo-inline">
<thead>
<tr>
<th >Edit</th>
    <th>Pořadí</th>
    <th>Smazat</th>
    <th>číslo</th> 
    <th>Typ</th>   
    <th>Kód</th> 
    <th>Kat.</th>   
    <th>Služba</th>
    <th>M. Jednotka</th>
    <th>Cena v Kč</th>
    <th>Cenová soustava</th>
</tr>
</thead>
<tbody id='thisTBL' class="row_position" >
<?php
$time = time();
if ($_GET['order'] == 1) {$ordr = 'serviceName';} else { $ordr = 'serviceOrder  ';}

   $query = "SELECT *,".$table_name.".ID as SID FROM $table_name  LEFT JOIN tbl_service_category ON tbl_service_category.ID = ".$table_name.".serviceCategory  ORDER BY " . $ordr;
$result = $GLOBALS["link"]->query($query);

if ($result && mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_array($result)) {
        ?>
         <tr  id="<?php echo $row['SID']; ?>" >
         <td  class='short center'> 
            <a href='index.php?id=<?php echo $page_name; ?>new&newsid=<?php echo $row['SID']; ?>' class="far fa-edit" ></a>
            </td>
            <td class='short center'  title='Přetáhněte položku myší'> <i class="xx fas fa-sort"></i> </td>
            <td class="short center">
            <a onclick="return confirm('Skutečně chcete položku vymazat z databáze?')" href="index.php?id=<?php echo $page_name; ?>&amp;delete=<?php echo $row['SID']; ?>" class="far fa-times-circle text-danger"></a>
            </td>
            <td class="short tinymce-2change" id='serviceNr--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['serviceNr']; ?> </td>
            <td class="short tinymce-2change" id='serviceType--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['serviceType']; ?> </td>
            <td class="short tinymce-2change" id='serviceCode--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['serviceCode']; ?> </td>
            <td class="short" data-toggle="tooltip" data-placement="top" title="<?php echo $row['catName'];  ?>"   ><?php echo $row['catCode'];  ?>   </td>
            <td class='tinymce-2change' id='serviceDesc--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['serviceDesc']; ?></td>
            <td class='tinymce-2change' id='serviceUnit--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['serviceUnit']; ?></td>
            <td class='tinymce-2change' id='servicePrice--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['servicePrice']; ?></td>
            <td class='tinymce-2change' id='servicePriceType--<?php echo $row['SID']; ?>--<?php echo $table_name; ?>'><?php echo $row['servicePriceType']; ?></td>
            
            </tr>
         <?php
}
}
?>
</tbody>
</table>
</div>

<a href="#" id="btnExport"> EXCEL </a>

<script type="text/javascript">


$(document).ready( function () {
        $('#tableOUT').DataTable( {
        paging: false ,
        
        
        "columnDefs": [
         { "orderable": false, "targets": 0 },
         { "orderable": false, "targets": 2 },
         { "orderable": false, "targets": 3 },
         { "orderable": false, "targets": 4 },
         { "orderable": false, "targets": 7 },
         { "orderable": false, "targets": 8 },
         { "orderable": false, "targets": 1 }
         ]
        

        } );
      } );
   



    $("#btnExport").click(function (e) {
        var htmltable= document.getElementById('tableOUT');
        var html = htmltable.outerHTML;
        while (html.indexOf('á') != -1) html = html.replace('á', '&aacute;');
        while (html.indexOf('Á') != -1) html = html.replace('Á', '&Aacute;');
        while (html.indexOf('č') != -1) html = html.replace('č', '&#269;');
        while (html.indexOf('Č') != -1) html = html.replace('Č', '&#268;');
        while (html.indexOf('é') != -1) html = html.replace('é', '&eacute;');
        while (html.indexOf('É') != -1) html = html.replace('É', '&Eacute;');
        while (html.indexOf('í') != -1) html = html.replace('í', '&iacute;');
        while (html.indexOf('Í') != -1) html = html.replace('Í', '&Iacute;');
        while (html.indexOf('ó') != -1) html = html.replace('ó', '&oacute;');
        while (html.indexOf('Ó') != -1) html = html.replace('Ó', '&Oacute;');
        while (html.indexOf('ú') != -1) html = html.replace('ú', '&uacute;');
        while (html.indexOf('Ú') != -1) html = html.replace('Ú', '&Uacute;');
        while (html.indexOf('º') != -1) html = html.replace('º', '&ordm;');
        while (html.indexOf('ň') != -1) html = html.replace('ň', '&#328;');
        while (html.indexOf('Ň') != -1) html = html.replace('Ň', '&#327;');
        while (html.indexOf('š') != -1) html = html.replace('š', '&#353;');
        while (html.indexOf('Š') != -1) html = html.replace('Š', '&#352;');
        while (html.indexOf('ř') != -1) html = html.replace('ř', '&#345;');
        while (html.indexOf('Ř') != -1) html = html.replace('Ř', '&#344;');
        while (html.indexOf('ť') != -1) html = html.replace('ť', '&#357;');
        while (html.indexOf('Ť') != -1) html = html.replace('Ť', '&#356;');
        while (html.indexOf('´') != -1) html = html.replace('´', '&#39;');
        while (html.indexOf('ě') != -1) html = html.replace('ě', '&#283;');
        while (html.indexOf('Ě') != -1) html = html.replace('Ě', '&#282;');
        while (html.indexOf('Ý') != -1) html = html.replace('Y', '&Yacute;');
        while (html.indexOf('ý') != -1) html = html.replace('ý', '&yacute;');
        while (html.indexOf('ž') != -1) html = html.replace('ž', '&#382;');
        while (html.indexOf('Ž') != -1) html = html.replace('Ž', '&#381;');
        var result = 'data:application/vnd.ms-excel,' + encodeURIComponent(html);
        this.href = result;
        this.download = "SLUZBY.xls";
        return true;
    });

    $( ".row_position" ).sortable({
        delay: 150,
        stop: function() {
            var selectedData = new Array();
            $('.row_position>tr').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
          }
    });
    $( ".xx" ).mouseenter(function() {
        $(".row_position").sortable("enable");
    }).mouseleave(function() {
        $(".row_position").sortable("disable");
    });

    $( ".tinymce-2change" ).mouseenter(function() {
        $(".row_position").sortable("disable");
    });

    function updateOrder(data) {
        console.log(data);
        $.ajax({
            url:'/inside/ajax-order.php?name=' + '<?php echo "serviceOrder--b--" . $table_name; ?>',
            type:'post',
            data:{position:data},
            success:function(){
                console.log(data);
            }
        })
    }


</script>